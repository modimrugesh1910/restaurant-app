# Paypal Restaurant App

## Development Build

1. `npm install`
2. `npm start`

## Production Build

1. `npm run build`

## Github - 
https://bitbucket.org/modimrugesh1910/restaurant-app/src

## Activities

* fetching data over http call
* sharing data over files
* sorting functionality
* Implement a feature to filter restaurants based on different kinds of cuisines.
* Implement functionality to search for restaurants by name.
* Implement multiple sort features based on Rating, Votes, and Average Cost for two.
* pagination
* theme changing
* version control
